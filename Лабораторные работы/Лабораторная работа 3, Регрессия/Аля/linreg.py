import numpy as np

class linReg:
    def __init__(self, learning_rate, iters):
        self.learning_rate = learning_rate
        self.iters = iters

    def fit(self, X, y):
        x = np.c_[np.ones(X.shape[0]), X]
        self.w = np.random.rand((x.shape[1])).reshape(-1,1)
        for i in range(self.iters):
            self.w = self.w - self.learning_rate * self.Gradient_MSE(x, y)

    def predict(self, X):
        x = np.c_[np.ones(X.shape[0]), X]
        return np.dot(x, self.w)

    def Gradient_MSE(self, X, y):
        return 2/X.shape[0] * np.dot(X.T, (np.dot(X, self.w) - y))  