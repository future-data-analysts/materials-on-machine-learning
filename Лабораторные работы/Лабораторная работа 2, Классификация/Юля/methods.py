import numpy as np
from collections import defaultdict


class KNN:

    def __init__(self, n):
        self.n = n

    def fit(self, X, y):
        self.X_train = X
        self.y_train = y

    def predict(self, X_test):
        predicted = np.zeros(X_test.shape[0], dtype=self.y_train.dtype)

        for i in range(X_test.shape[0]):
            distances = np.linalg.norm(self.X_train - X_test[i], axis=1)
            neighbours, neighbouring_distances = self.find_n_neighbours(distances)
            best_class = self.find_best_class(neighbours)
            predicted[i] = best_class
        return predicted

    def find_n_neighbours(self, distances):
        neighbours_distances = distances.argsort()[:self.n]
        neighbours = self.y_train[neighbours_distances][:self.n]
        return neighbours, neighbours_distances

    def find_best_class(self, neighbours):
        classes, counts_elem_classes = np.unique(neighbours, return_counts=True)
        max_ = counts_elem_classes.max()
        best_class = 0
        for i in range(len(counts_elem_classes)):
            if counts_elem_classes[i] == max_:
                best_class = classes[i]
                pass
        return best_class


class NB:
    
    def __init__(self):
        self.feature_count = defaultdict(lambda: 0)  # частота признака
        self.class_count = defaultdict(lambda:0) # частота класса

    def fit(self, X, y):
        # вычисление частот классов и признаков
        for feature, label in zip(X, y): # берем признаки и метки 
            self.class_count[label] += 1   # в словарь частоты классов добавляем метки
            for value in feature:
                self.feature_count[(value, label)] += 1 # в словарь частоты признаков добавляем признаки

        # нормализация значений
        count_samples = len(X)  # количество объектов в обучении
        for k in self.class_count: # работаем с элементами из словаря частоты классов
            self.class_count[k] /= count_samples # нормализуем значения
            
        # доля объектов с данным значением признака среди объектов данного класса
        for value, label in self.feature_count:
            self.feature_count[(value, label)] /= self.class_count[label]
        
        return self
    
    def predict(self, X):
    # возвращение argmin классов 
        return min(self.class_count.keys(), key=lambda c : self.calculate_class_count(X, c)) 
    
    def calculate_class_count(self, X, c):
        # вычислиние частоты для текущего класса
        v = - np.log(self.class_count[c])
        for feature in X: 
            v += - np.log(self.feature_count.get((feature, c), 10 ** (-7)))
        return v


